# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=spacebar
pkgver=0_git20210122
pkgrel=0
_commit="7303da304589affbdefc1f3eae7628ed7f78458a"
pkgdesc="Collection of stuff for running IM on Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/spacebar"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
license="GPL-2.0-or-later AND LicenseRef-KDE-Accepted-GPL"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kcontacts-dev
	ki18n-dev
	kirigami2-dev
	knotifications-dev
	kpeople-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	telepathy-qt-dev
	"
source="https://invent.kde.org/plasma-mobile/spacebar/-/archive/$_commit/spacebar-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="e2c7c297201af004f3ea736c8d56d6130101e868bf085e3650d7c1b011ef80f496e6a0b6a685abc85b3fae944f07244844d63f3af6b219456e6e7373396cf1a7  spacebar-7303da304589affbdefc1f3eae7628ed7f78458a.tar.gz"
