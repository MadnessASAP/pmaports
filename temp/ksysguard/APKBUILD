# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksysguard
pkgver=5.20.90
pkgrel=0
pkgdesc="Track and control the processes running in your system"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x !mips64"
url="https://userbase.kde.org/KSysGuard"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	kwindowsystem-dev
	libksysguard-dev
	libnl3-dev
	lm-sensors-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/ksysguard-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ksystemstatstest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="41f333afb89c2a8bb0f22a439a16cbc3080892416bae30711009899e49ae7b93e98b6d00cdc92b01520f0bb7b907bda6c23d3522026d4acd70bafcb12faaa638  ksysguard-5.20.90.tar.xz"
